### 一、项目介绍
本项目为本人学习Linux网络编程的产物，仿照游双老师的书籍《Linux高性能网络服务器编程》修改的demo

### 二、项目启动
#### 1、服务器测试环境
Ubuntu版本20.04、MySQL版本5.7.28

#### 2、浏览器测试环境
Windows、Linux均可。Chrome，其他浏览器暂无测试

#### 3、测试前确认已安装MySQL数据库
```
// 建立yourdb库
create database yourdb;

// 创建user表
USE yourdb;
CREATE TABLE user(
    username char(50) NULL,
    passwd char(50) NULL
)ENGINE=InnoDB;

// 添加数据
INSERT INTO user(username, passwd) VALUES('name', 'passwd');
```
#### 4、修改main.cpp中的数据库初始化信息
```
//数据库登录名,密码,库名
string user = "root";
string passwd = "root";
string databasename = "yourdb";
```
#### 5、运行编译脚本
```
sh ./build.sh
```

#### 6、启动server

```
./bin/server
```
#### 7、浏览器端
```
http://IP:8080/
```